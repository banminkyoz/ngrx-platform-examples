import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TestComponent } from './test.component';

import { CounterComponent, LanguageSwitcherComponent } from '../../lib/components/';

const adminRoutes: Routes = [
    {
        path: '',
        component: TestComponent,
        children: [
            {
                path: '',
                children: [
                    { path: 'counter', component: CounterComponent }
                ]
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(adminRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class TestRoutingModule { }
