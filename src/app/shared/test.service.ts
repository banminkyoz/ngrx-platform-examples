import { Injectable } from '@angular/core';

@Injectable()
export class TestService {
  private _text: string;
  private _counting: number;

  constructor() {
    this._text = 'Pham';
    this._counting = 0;
  }

  public get text(): string {
    this._counting++;
    console.log('test_couting', this._counting);
    return this._text;
  }
  public set text(v: string) {
    this._text = v;
  }

}
