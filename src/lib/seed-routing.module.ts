import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
  MainComponent,
  CounterComponent,
  LanguageSwitcherComponent,
  PostObjectComponent
} from './components';


const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        children: [
          { path: 'counter', component: CounterComponent },
          { path: 'language', component: LanguageSwitcherComponent },
          { path: 'post', component: PostObjectComponent }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SeedRoutingModule { }
