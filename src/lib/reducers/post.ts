import * as PostActions from '../actions/post';
import { Post } from '../models/post';

export type Action = PostActions.All;
/// Default app state
const defaultState: Post = {
  text: 'Hello. I am the default post',
  likes: 0
}
/// Helper function to create new state object
const newState = (state, newData) => {
  return Object.assign({}, state, newData)
}
/// Reducer function
export function postReducer(state: Post = defaultState, action: Action) {
  console.log(action.type, state)
	switch (action.type) {
  		case PostActions.POST_EDIT_TEXT:
  			return newState(state, { text: action.payload });
      case PostActions.POST_UPVOTE:
        return newState(state, { likes: state.likes + 1 });
  		case PostActions.POST_DOWNVOTE:
  			return newState(state, { likes: state.likes - 1 });
  		case PostActions.POST_RESET:
  			return defaultState;
  		default:
  			return state;
	}
}
