import { Action } from '@ngrx/store';

export const LANGUAGE_SPANISH = 'SPANISH';
export const LANGUAGE_FRENCH  = 'FRENCH';
export const LANGUAGE_ENGLISH = 'ENGLISH';

export function languageSwitcher(state: string, action: Action) {
  switch (action.type) {
    case LANGUAGE_FRENCH:
      return state = 'Bonjour le monde';
    case LANGUAGE_SPANISH:
      return state = 'Hola Mundo';
    case LANGUAGE_ENGLISH:
      return state = 'Hello World';
    default:
      return state = 'Default Language';
  }
}
