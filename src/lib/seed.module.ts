import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import 'rxjs/add/observable/fromEvent';

/* Angular dependencies */
import {
  MatToolbarModule,
  MatButtonModule,
  MatIconModule,
  MatInputModule,
  MatFormFieldModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

/* User dependencies */
import { SeedRoutingModule } from './seed-routing.module';

import {
  MainComponent,
  CounterComponent,
  LanguageSwitcherComponent,
  PostObjectComponent
} from './components';

import { SampleDirectiveDirective } from './directives';
import { SamplePipePipe } from './pipes';
import { SampleServiceService } from './services';

// NGRX
import { StoreModule } from '@ngrx/store';
import { counterReducer, languageSwitcher, postReducer } from '../lib/reducers';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    SeedRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    StoreModule.forRoot(
      {
        counter: counterReducer,
        language: languageSwitcher,
        post: postReducer
      }
    )
  ],
  declarations: [
    MainComponent,
    CounterComponent,
    LanguageSwitcherComponent,
    PostObjectComponent,
    SampleDirectiveDirective,
    SamplePipePipe,
  ],
  providers: [SampleServiceService]
})
export class SeedModule { }
