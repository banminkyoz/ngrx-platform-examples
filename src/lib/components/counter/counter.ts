import {
  COUNTER_INCREMENT,
  COUNTER_DECREMENT,
  COUNTER_RESET
} from './../../reducers';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

interface AppState {
  counter: number;
}

@Component({
  selector: 'app-counter',
  templateUrl: './counter.html',
  styleUrls: ['./counter.scss']
})
export class CounterComponent implements OnInit {

  counter: Observable<number>;

  constructor(private _router: Router, private store: Store<AppState>) {
    this.counter = store.select('counter');
  }

  ngOnInit() {
  }

  goNext() {
    this._router.navigate(['/redux/language']);
  }

  increment() {
    this.store.dispatch({ type: COUNTER_INCREMENT });
  }

  decrement() {
    this.store.dispatch({ type: COUNTER_DECREMENT });
  }

  reset() {
    this.store.dispatch({ type: COUNTER_RESET });
  }
}
